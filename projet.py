# introduction
print('Welcome to Jesika NURAMAMI Python projects!')
print('There are 5 projects inside this program')
print('Chatbot, Geography Trivia Game, IDR to USD Real time currency converter')
print ('web scrapping from Fossil website, and URL to QR Code Generator')
print ('Enjoy! Please type anything to call your chatbot')


# conversation_bot_intro
userGreeting = input()
print('Hi, whats your name?')
name = input()
print('Hi ' + name)
print ('How are you?')
userCondition = input()
if 'fine' not in userCondition :
  print('Why?')
else : print('Good, hope you enjoy your day')
print('what are you doing now?')

# conversation bot first chat
imFine = input()
if 'nothing' in imFine :
  print('Would you like to play a game with me? YES or NO')
else : print('Great, it looks fun! Would you like to play a game with me? YES or NO')
gameMe = input()
if 'YES' in gameMe :
  print('lets play geography trivia quiz with me!')
else : print('No problem, Good bye and have a blast day!')

# Geography Trivia Game_intro #
print('============================')
print('Hello, Welcome to Geography Trivia Game! Have fun!')
print ('Are you ready to play? Please answer with YES or NO')
readyAns = input()
if 'YES' in readyAns :
  print('Great! Lets go!')
else: print('Goodbye, see you soon!')

# Geography Trivia Game_question1 #
print('one score for one correct answer, and you should use CAPITAL LETTERS to submit an answer. Good luck!')
score = 0
total_q = 5
print('What is the capital city of Japan?')
answer1 = input()
if 'TOKYO' in answer1 :
  score +=1
  print('Correct!')
else : print('Sorry, wrong answer')

# Geography Trivia Game_question2 #
print('What is the capital city of Germany?')
answer2 = input()
if 'BERLIN' in answer2 :
  score +=1
  print('Correct!')
else : print('Sorry, wrong answer')

# Geography Trivia Game_question3 #
print('What is the capital city of Thailand?')
answer3 = input()
if 'BANGKOK' in answer3 :
  score +=1
  print('Correct!')
else : print('Sorry, wrong answer')

# Geography Trivia Game_question4 #
print('What currency does France use?')
answer4 = input()
if 'EURO' in answer4 :
  score +=1
  print('Correct!')
else : print('Sorry, wrong answer')

# Geography Trivia Game_question5 #
print('What season is december in australia?')
answer5 = input()
if 'SUMMER' in answer5 :
  score +=1
  print('Correct!')
else : print('Sorry, wrong answer')

# Final score #
print('Good job! Thank you for playing, you got ', score, ' question correct.')
mark = (score/total_q) * 100

print('mark ', mark)

print('============================')

# BCA Currency converter
# creating a currency converter from IDR (Indonesian Rupiah) to USD (Dollar)
# Each bank in Indonesia use its own exchange rates.
# IDR is started from 1000, 10 000, 100 000, so on.
# Since Jesika is a BCA Bank customer, she wants to create her own currency converter.

from bs4 import BeautifulSoup as bs
import requests

print('Currency Converter from IDR (Indonesian Rupiah) to USD (Dollar)')
print('Each bank in Indonesia use its own exchange rates')
print('IDR is started from 1000, 10 000, 100 000, so on')
print('This is a currency converter for BCA Bank customer using BCA Bank Real time Exchange rates')

class RealTimeCurrencyConvertor():
  def __init__(self,url):
    data = requests.get(url).content
    rates = bs(data, "html.parser").find_all(class_="m-table-kurs--sticky")
    self.rates = {}
    for rate in rates:
        title = " ".join(rate.find(class_="sticky-col first-col").stripped_strings)
        price = rate.find(class_="p-16 a-text a-text-body a-text-normal-heading tb-cell va-middle").text.strip()
        self.rates[title] = price
    print(self.rates)
         
  def convert(self, from_currency, to_currency, amount):
    print(self.rates)
    initial_amount = float(amount.replace(",", ""))
    rate = float(self.rates[to_currency].replace(",", ""))
    amount = round(initial_amount / rate, 2)
    print('{} {} = {} {}'.format(initial_amount, from_currency, amount, to_currency))

url = 'https://www.bca.co.id/en/informasi/kurs' 
converter = RealTimeCurrencyConvertor(url)
from_country = input("From Country: ")
to_country = input("TO Country: ")
amount = input("Amount: ")

print(converter.convert(from_country,to_country,amount))
print('thank you for using our service, see you next time')
print('============================')
print('It looks like you got some money! Lets shopping!')
print('Welcome to Fossil Women Bag Catalog!')

# import dataset from a website

import requests
import pandas as pd
from bs4 import BeautifulSoup

url = 'https://www.fossil.com/fr-fr/sacs/sacs-a-main-pour-femme/sacs-a-dos/?m=g'
uagent = ('Chrome/102.0.5005.61')
headers = {"User-Agent": uagent}
resp = requests.get(url, headers=headers)
html_parser = BeautifulSoup(resp.content, "html.parser")
results = []

# Find Women Backpack with Fossil brand
backpacks = html_parser.find_all(class_="product-tile")
for backpack in backpacks:
  title = " ".join(backpack.find(class_="link product-name-link").stripped_strings)
  price = backpack.find(class_="prices-container").text.strip()
  results.append({"title": title, "price": price})

# Save in xlsx
df = pd.DataFrame(results)
xlsx_file = "FossilPricelist.xlsx"
df.to_excel(xlsx_file, index=False)
print(f'{df.shape} df saved as {xlsx_file}')# import dataset from a website

import requests
import pandas as pd
from bs4 import BeautifulSoup

url = 'https://www.fossil.com/fr-fr/sacs/sacs-a-main-pour-femme/sacs-a-dos/?m=g'
uagent = ('Chrome/102.0.5005.61')
headers = {"User-Agent": uagent}
resp = requests.get(url, headers=headers)
html_parser = BeautifulSoup(resp.content, "html.parser")
results = []

# Find Women Backpack with Fossil brand
backpacks = html_parser.find_all(class_="product-tile")
for backpack in backpacks:
  title = " ".join(backpack.find(class_="link product-name-link").stripped_strings)
  price = backpack.find(class_="prices-container").text.strip()
  results.append({"title": title, "price": price})

# Save in xlsx
df = pd.DataFrame(results)
xlsx_file = "FossilPricelist.xlsx"
df.to_excel(xlsx_file, index=False)
print(f'{df.shape} df saved as {xlsx_file}')

# URL Converter to QR Code or QR Code Generator
import qrcode
from PIL import Image

print('============================')
print('Welcome to URL QR Code Generator, please input any URL to generate your QR Code!')

URL = input()
img = qrcode.make(URL)

print('Now please input your QR Code name')
QRCodeName = input()
img.save(QRCodeName)

print('Thank you for using our QR Generator, please check your folder to find your QR Code!')
